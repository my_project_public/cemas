part of 'pages.dart';

class TentangPage extends StatelessWidget {
  const TentangPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              height: 255,
              width: double.infinity,
              decoration: const BoxDecoration(
                color: mainColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Sistem Pakar',
                            style: whiteSemiFont.copyWith(fontSize: 18),
                          ),
                          const SizedBox(height: 8),
                          Text(
                            'Gangguan Kecemasan\nPada Remaja',
                            style: whiteMediumFont.copyWith(fontSize: 14),
                          ),
                        ],
                      ),
                      const SizedBox(width: 22),
                      Image.asset('assets/illustration.png',
                          height: 140, width: 95),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: const [
                  Text(
                    "The Doctor Society",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  SizedBox(height: 24),
                  Text(
                      "SIstem Pakar merupakan aplikasi yang dapat membantu kinerja konselor sebaya dalam mendiagnosis awal penyakit mental gangguan kecemasan (Anxiety Disorder).Sistem Pakar ini terbatas dengan kemampuan dan pengalam tiga psikolog yang berada di Nusa Tenggar Barat. Sistem ini hanya dapat mendiagnosis awal dengan bimbingan konselor sebaya dalam PIK-R. Tidak dianjurkan untuk melakukan self-diagnose atau mendiagnosa diri sendiri tanpa bimbingan dari konselor sebaya. Sistem ini sudah diuji berdasarkan beberapa tahapan pengujian dan layak untuk menjadi aplikasi pengganti pakar di PIK-R di Nusa Tenggara Barat.",
                      style: TextStyle(fontSize: 13)),
                ],
              ),
            ),
            const SizedBox(height: 40)
          ],
        ),
      ),
    );
  }
}
